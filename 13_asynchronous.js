// I won't go with examples here but simply, this is how JS is handling asynchronous stuffs.

function clickHandler() {
  console.log("clicked!");
}

document.addEventListener("click", clickHandler);

function a() {
  console.log("loading started.");
  var ms = 3000 + new Date().getTime();
  while (new Date() < ms) {}
  console.log("finished the timer.");
}

a();
console.log("finished whole global execution");

/* 
So what I did, is I ran the above example, and I cliked 2 times on the screen instantly after loading it. This is what I got in the console: 
loading started
finished the timer
finished whole global execution
clicked!

The above examples basically executes the a() function at the bottom, and in a() we got a while loop that makes the browser wait for 3 seconds.
So we have loading started when calling a(), then wait of 3 seconds, and then "finished the timer" after that. and lastly we have "finished whole global exection", and on the VERY END we have the "clicked!" registrations, even tho we did them on the start.

WHY IS THAT?
Well global calls a(); a() makes the browser wait, prints 2 times stuff, closes the a execution context when done, then prints the global console, then closes the global execution stack, and finally it checks the browser queue if there are any events that are waiting, and creates additional execution contexts in the execution stack, if the item in queue needs it.

So, this is why JS is synchronous and not asynchronous.
Because no matter what happens out of events and stuff, until JS clears the execution stack that it is required by the code, it won't look at any events.

The Asynchronoiusity is added by a browser feature, that handles events, and creates a queue for the JS engine, whenever the execution stack is done. Then it checks for any event that is in the queue or some http data that has been fetched by some function "asynchronously", etc. Then it completes all the execution contexts requied for that queue item, and when that is done, it goes to the next one.

Always one task at a time.
*/
