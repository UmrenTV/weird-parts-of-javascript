function a() {
  function b() {
    console.log(myVar);
    // If I was to call this.myVar here, it will refernce the myVar that is attached to the global this object, which is the one that is globally defined directly in the .js file. not in function.
    // console.log(this.myVar) - like here.
  }
  var myVar = 2;
  b();
}

var myVar = 1;
a();
// b();

// calling here b(); won't work, because in the first phase of parsing this .js file, the engine can't find b(); since it only stores what is needed for the first execution context, which is the global. It doesn't go inside the functions, it just checks what is required to be in the memory, for the global execution conext to be created and executed.
// When global context is done creating and it's being executed, then a() will create the same as the global did. 1 phase of creation, and then the phase of execution. In the first phase will find that there is b(); and will put it in memory and in the second phase will execute b() and create execution context for b(). THen it does the 2 phses for b(), and so on. In the first phase of the creation of the global context, it litteraly doesn't know that b() exists. Neither in the second, since b() is not essential for the global context to be executed. It is essential for a(), but that is later.

// Because of this way of working, we can't access from the global execution context, the function b directly, it has to be called from the function that is it's lexical context (or outter enviroment).

// When it comes to accessing varibles, it goes by lexical context. So if you run b() from a(), what happens is, b() can't find the varibale declared inside of b() that is trying to console.log it, so it goes to the outter enviroment (lexically) and checks if it's there. in this case a() is lexically where b sits. So checks a() and it finds it. It will print result 2 in console.

// If myVar was not defined and declared in a(), it will go down the scope chain lexically, and since a() sits lexically inside the global context, it will seaarch if myVar is available globally. If it doesn't find it, it will throw an error "myVar is not defined", but since here finds it in the global context, then it will print out 1 in the console.

// Last thing, if we move b() outside of a(), and we position is in the global context, whenever a() calls b(), it will print 1, since the outter enviroment for b() is the global context, therefore it finds it there, and it prints the value that the global context assigned to it.

// Something like here:

/*
function b() {
    console.log(myVar);
  }

 function a() {
  
  var myVar = 2;
  b();
}

var myVar = 1;
a(); */

// Another way of declaring variable is let.
// let is very similar as var, but it's scoped to a block. (usually blocks are surrounded by curly braces or {})
// the variable assigned with let, are only available inside that block where it's been created, and only while that block is being executed. Until the block is in it's execution phase, you can't access that variable. This is true even for for loops.
// For every iteration of for loop, the let variables are getting new spot in the memory, and the previous one is not available anymore.

/* 
function a() {
  let a = 1;
  console.log(b); - This throws error.
  function b() {
    let b = 2;
    console.log(a); - This prints 1.
  }
}
*/
// Better way for doing it:
function c() {
  let dPlaceholder = 1; // this is what I call helper variable. To help you access variable in function that usually is not availaboe.
  d();
  function d() {
    let d = 2;
    dPlaceholder = d; // This is how you access d, by passing it's value to a helper variable in the outter enviroment.
  }
  console.log("down", dPlaceholder); // this prints 2.
}

c();
