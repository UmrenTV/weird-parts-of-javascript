console.log(1 < 2 < 3);
// the above statement returns true.
console.log(3 < 2 < 1);
// this statements returns true too!
/* 
WHY? Well, by the laws of precendence and associativity, the < operators are the same, and have same presendence. The associativity says that they run left-to-right if the same precendence is met. So we got this:
(Below is not actual syntax you can use, it's just random jibberish to explain a point)
console.log(3<2) < 1 so it's:
console.log(false < 1)

So since you give 2 parameters of different type to the < built-in function, in order to calculate the result, it coerces the false value to a number format.

So if we run the built-in method Number(false), it will coerce the false value to 0, same as it would coerse true to 1.

So what happens with above statement is
console.log(0 < 1), which indeed it true.

It is 100% that it should give false, by human readings, but by JavaScript engine, it is completely logical, given the laws of cohersion, presedence, and associativity. So if one is not aware of this benig possible, can end up in a lot of bugs, sometimes.

the first line would be:
1 < 2 = true and Number(true) is 1.
so 1 < 3 is also true, therefore whole statement is true.
*/

var a;
a = 0;

if (a) {
  console.log("something is there.");
}

if (a || a === 0) {
  console.log("either something is there, or a is 0.");
}

// ********************************************************
// Lesson on default values
//*****************************************************

// If called the function || like this, it will return the value that is first on the left, and is coercable to true. so undefined || 3 will return 3. Coz undefined means lack of existance or something so it's false. Same for null, 0 or empty string.
function c(name) {
  name = name || "[this coerced to false :( ]";
  console.log("Hello " + name);
}
c(); // by default we know that in first cycle of execution context, it reserves memory slot for variables, and sets them to undefined. If you don't give them value later, they'll stay undefined. And undefined means, lack of value. Null is about the same, except it doesn't have type too. Undefined is of undefined type, which is a type actually. 0 is a value, and "" is empty value. But both of them have type, they coerce to false, because their value is lowest it can be, so it returns false.
c(undefined);
c(null);
c(0);
c("");
c("Jane");
c(3);
c(1);

// *******************************************************
// LESSON ON CHECKING & ASSIGNING VARIABLES
//*******************************************************

var libraryName = window.libraryName || "newLibrary";

/* Usually when are using a library, we are just importing them into our html as <script src="x" then the other then the other. And it might seem that they are 3 files, but at the end when the whole javascript code is run, these are considered as one global execution context. They don't have separate context, and therefore they are all attached to the same global object.
so if you have 3 libraries that you are adding from 3 different authors, and they are all using libraryName as variable name, without considering the fact that someone might be using it in another library, without the check above, it will do name clash in the global .js file that is being created at the end, and it might cause whole bunch of errors.

In order to avoid that when creating a library, we can use the above check to see if there is already a variable that is named that way and if there is, then use that value, if not assign the new one. 
It is good practice and also a neat trick when doing soemthing like framework or library in the future. */
